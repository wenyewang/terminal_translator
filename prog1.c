#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/wait.h>

#define MAXLINESIZE 100
#define MAXWORDS 16
#define MAXWORDLEN 64

extern char **environ;
char current_line[MAXLINESIZE+1];
char *words[MAXWORDS];
int num_words;
char path[MAXWORDLEN]; //path to the command file

int cmd_pos = 0;		// position of command in words
char last_ret;			// ret value of last process
int gArgc;				// the number of cmd + argument
char* gArgv[MAXWORDS];	// store the arguments passed to command

void resetVal()
{
	cmd_pos = 0;
	num_words = 0;
}

// Read from standard input and return lens of line when line is read
int readLine()
{
  char c; // current character read from read call
  int status; //status of read call
  int len=0; // length of string read
  int noblank=0; // set true if non blank character is seen

  while(1)
  {
    status = read(0, &c, 1);
    if ( status == 0)
      _exit(1); //end of file
    if ( status == -1)
      _exit(1); // read error
    write(1, &c, 1);
    
    if (c !=' ' || c != '\t')
      noblank = 1;

    if (c == '\n')
      break; // end of line
      
    current_line[len++] = c;
  }

  if (noblank == 0 || len == 0) // line contains only blank char
    return 0;
  current_line[len] = '\0';
  return len;
}

// parses the input line
// returns 1 on success and 0 otherwise
int parseLine()
{
  char *word;
  
  num_words = 0;

  word = strtok(current_line, " \t");
  while( word != NULL)
  {
    words[num_words++] = word;
    word = strtok(NULL," \t");
  }
  return 1;
}

// checks if commmand is just a file name or
// a relative path or absolute path and then verifies
// if it is executable and updated path variable with
// path to the file.
// returns 1 if executable else 0
int executable()
{
  char *p;
  char *pathenv;
  char msg[100];
  int i;

  gArgc = 0;
  // if it is already relative or absolute path
  if(strchr(words[cmd_pos], '/') != NULL)
  {
    strcpy(path, words[0]);
    if( access(path, X_OK) == 0) {
      for(i = cmd_pos; i < num_words; i++) {
      	if(strcmp(words[i], ";") == 0 || strcmp(words[i], "&&") == 0 || strcmp(words[i], "||") == 0) {
      		//mark end of argument array
      		gArgv[gArgc] = NULL;
      		break;
      	}
      	gArgv[gArgc++] = words[i];
      }
      strcpy(path, gArgv[0]);
      return 1;
    }
    else
      return 0;
  }

  pathenv = strdup(getenv("PATH"));
  p = strtok(pathenv, ":");	//get first directory in path
  while( p!= NULL)
  {
    strcpy(path, p);
    strcat(path, "/");
    strcat(path, words[cmd_pos]);
    if(access(path, X_OK) == 0)
    {
      free(pathenv);
      
      // if the command is valid. then we put argument into gArgv[];
      for(i = cmd_pos; i < num_words; i++) {
      	if(strcmp(words[i], ";") == 0 || strcmp(words[i], "&&") == 0 || strcmp(words[i], "||") == 0) {
      		//mark end of argument array
      		gArgv[gArgc] = NULL;
      		break;
      	}
      	gArgv[gArgc++] = words[i];
      }
      return 1;
    }
    p = strtok(NULL, ":");
  }
  free(pathenv);
  
  sprintf(msg,"program file not found '%s'\n", words[cmd_pos]);
  write(1,msg,strlen(msg));
  
  return 0;
}

// Try to execute the command
void executeCmd()
{
  int status, which;
  int fd;
  pid_t pid;
  char msg[100];

  while(executable()) //run if command is executable
  {  	
    pid = fork(); // create a new process
    if(pid == -1) // did fork succeed
    {
      write(1,"fork failed.\n",12);
      exit(1);
    }

    if( pid == 0) // in child
    {
      last_ret = execve(path, gArgv, environ);

      perror("excve");
      _exit(0);
    }
    if (pid !=0)
    {
      //In parent
      which = wait(&status);
      if( which == -1)
      {
        write(1,"wait failed.\n",12);
        exit(1);
      }
      if (status & 0xff) { /* abnormal termination */
        sprintf(msg,"process %d terminated abnormally for reason %d\n", which, status & 0xff);
        write(1,msg,strlen(msg));
      }
      last_ret = (status >> 8) & 0xff; ///< last_ret is bit 15-8 of status.
      
      // determine how to do when get last_ret
      if(cmd_pos + gArgc < num_words && strcmp(words[cmd_pos + gArgc], ";") == 0) {
      	// do nothing keep decode & run
      }
      else if(cmd_pos + gArgc < num_words && strcmp(words[cmd_pos + gArgc], "&&") == 0) {
      	// if last_ret == 0 keep running, else stop
      	if(last_ret == 0) {
      	}
      	else {
      		break;
      	}
      }
      else if(cmd_pos + gArgc < num_words && strcmp(words[cmd_pos + gArgc], "||") == 0) {
      	// if last_ret != 0 keep running, else stop
      	if(last_ret != 0) {
      	}
      	else {
      		break;
      	}
      }
      else if(cmd_pos + gArgc < num_words) {
		sprintf(msg,"error delimier %s\n", words[cmd_pos + gArgc]);
		write(1,msg,strlen(msg));
      }
	  cmd_pos += gArgc + 1;
	  if(cmd_pos >= num_words) {
	  	// if cmd_pos is out of boundry of words, quit the while.
	  	break;
  	  }
    }
  }
}

int main()
{
  char msg[100];
  sprintf(msg, "Hello, Welcome to the KSHISHELL\n");
  write(1, msg, strlen(msg));

  int len = 0;
  while(1){
  	resetVal();
  	if(readLine() && parseLine())
      executeCmd();
  }
  return 0;
}
